# [Startup Web Design](https://andrea-code.gitlab.io/startup-template/)

![web_formacion](https://user-images.githubusercontent.com/37070594/47854047-ff462500-dde0-11e8-9060-39f430c1cc10.gif)

Diseño de una pagina web responsive para una Startup, escuela o negocio.
- Tecnologias usadas: 
  - Javascript
  - CSS + SASS
  - Html

[Visualizar la pagina web](https://andrea-code.gitlab.io/startup-template/)
